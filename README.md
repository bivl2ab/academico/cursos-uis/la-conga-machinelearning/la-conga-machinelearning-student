# INTRODUCCIÓN A MACHINE LEARNING. LA-CoNGA

## Bienvenidos!


<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/la-conga-dataanalysis/la-conga-da-student/-/raw/master/imgs/banner_DA.png"  width="1000px" height="200px">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb)

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local.


## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres se desarrollará  dentro de las fechas establecidas en el cronograma.



## Calendario y plazos

                        SESSION 1                       SESSION 2


     W01 jul04-jul06    Intro-ML [Pres]                 Intro-ML [Pres]
     W02 jul11-jul13    Intro ML-class [N1]             Intro ML-class [N1]
     W03 jul18-jul20    ML Clas splits [N2]             ML Clas metric [N2]
     W04 jul25-jul27    Machine Learning Methods [N3]   Machine Learning Methods [N3]
     W05 ago01-ago03    ML Regression                   ML Regression
     W06 ago08-ago10    DL-intro-Grad                   DL-intro-forward
     W07 ago15-ago17    DL-intro-Back                   DL-DNN-aplications
     W08 ago22-ago24    DL-DNN-aplications              DL-DNN-aplications
